# Gifski

Docker images with [gifski](https://github.com/ImageOptim/gifski)

## Supported tags and respective Dockerfile links

- `1.3.2`, `latest` [(1.3.2/Dockerfile)](1.2.6/Dockerfile)
- `1.2.6` [(1.2.6/Dockerfile)](1.2.6/Dockerfile)
